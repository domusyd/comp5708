const CDP = require('chrome-remote-interface');
const chromeLauncher = require('chrome-launcher');

function launchChrome(headless=true) {
  return chromeLauncher.launch({
    port: 9222, // Uncomment to force a specific port of your choice.
    chromeFlags: [
      '--window-size=412,732',
      '--disable-gpu',
      headless ? '--headless' : ''
    ]
  });
}


(async function() {

  const url = process.argv[2]

  const chrome = await launchChrome();
  const protocol = await CDP({port: chrome.port});

  const {Page, Runtime} = protocol;
  await Promise.all([Page.enable(), Runtime.enable()]);


Page.navigate({url: url});

// Wait for window.onload before doing stuff.
Page.loadEventFired(async () => {
  const result = await Runtime.evaluate({
      expression: 'document.documentElement.outerHTML'
  });
  const html = result.result.value;
  console.log(html);

  protocol.close();
  chrome.kill(); // Kill Chrome.
});

})();
