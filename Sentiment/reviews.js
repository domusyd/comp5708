const CDP = require('chrome-remote-interface');
const chromeLauncher = require('chrome-launcher');
const cheerio = require('cheerio');

var reviewHTML = '';

function launchChrome(headless=true) {
  return chromeLauncher.launch({
    // port: 9222, // Uncomment to force a specific port of your choice.
    chromeFlags: [
      '--window-size=412,732',
      '--disable-gpu',
      headless ? '--headless' : ''
    ]
  });
}


//console.log(purl)
//process.exit()

(async function() {

  const purl = process.argv[2]
  var durl = purl.replace("/dp/",'/product-reviews/')

  //console.log(durl)

  const chrome = await launchChrome();
  const protocol = await CDP({port: chrome.port});

  const {Page, Runtime} = protocol;
  await Promise.all([Page.enable(), Runtime.enable()]);

 url = durl
 url = url + '?pageSize=100'

Page.navigate({url: url});

// Wait for window.onload before doing stuff.
Page.loadEventFired(async () => {

  const reviews = await Runtime.evaluate({expression: 'document.getElementById("cm_cr-review_list").innerHTML'});

  const reviewHTML = reviews.result.value;
  //console.log(reviewHTML)
  $ = cheerio.load(reviewHTML);
  var review_items = []
  $('[data-hook="review"]').each(function () {
    text = $(this).find('.review-text').text();
    date = $(this).find('.review-date').text();
    date = date.slice(3)
    title = $(this).find('.review-title').text();
    stars = $(this).find('.a-icon-alt').text();
    stars = stars.substring(0,3)
    var review = {
      'title' : title,
      'date' : date,
      'text' : text,
      'stars' : stars
    }
    review_items.push(review)
  });
  console.log(text)
 //console.log(JSON.stringify(review_items))
 //console.log(review_items.length)

  protocol.close();
  chrome.kill(); // Kill Chrome.
});

})();
