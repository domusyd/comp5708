package ie.moguntia.webcrawler;
import ie.moguntia.threads.*;
import java.net.*;

public class PSucker implements MessageReceiver {
	public PSucker(Queue q, int maxLevel, int maxThreads)
		throws InstantiationException, IllegalAccessException {
		ThreadController tc = new ThreadController(PSuckerThread.class,
												   maxThreads,
												   maxLevel,
												   q,
												   0,
												   this);
	}

	public void finishedAll() {
		// ignore
	}

	public void receiveMessage(Object o, int threadId) {
		// In our case, the object is already string, but that doesn't matter
		//System.out.println("[" + threadId + "] " + o.toString());
	}

	public void finished(int threadId) {
		//System.out.println("[" + threadId + "] finished");
	}

	public static void main(String[] args) {
		try {
			int maxLevel = 10;
			int maxThreads = 3;
			int maxDoc = -1;
			
			if (args.length >= 1) {
				URLQueue q = new URLQueue();				
				q.setMaxElements(maxDoc);
				q.push(new URL(args[0]), 0);
				new PSucker(q, maxLevel, maxThreads);
				return;
			}
			
		} catch (Exception e) {
			System.err.println("An error occured: ");
			e.printStackTrace();
			// System.err.println(e.toString());
		}
		System.err.println("Usage: java PSucker <url> [<maxLevel> [<maxDoc> [<maxThreads>]]]");
		System.err.println("Crawls the web for jpeg pictures and mpeg, avi or wmv movies.");
		System.err.println("-1 for either maxLevel or maxDoc means 'unlimited'.");
	}
}
