package ie.moguntia.webcrawler;

import ie.moguntia.threads.*;
import java.net.*;
import java.io.*;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

public class PSuckerThread extends ControllableThread {
	public void process(Object o) {
		// The objects that we're dealing with here a strings for urls
		try {
			URL pageURL = (URL) o;

			FileWriter fw = new FileWriter("/Users/dlovell/desktop/amz.csv", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);
			
			String rawPage = SaveURL.getURL(pageURL, out); 
			//System.out.println(rawPage.length() + " bytes received");
            Map<String, String> parents = new TreeMap<String,String>();
            Map<String, String> rssLinks = new TreeMap<String,String>();
			// treat the url a a html file and try to extract links
            
            
            
            Map<String,String>  links = SaveURL.extractLinksWithJSoup(pageURL, rawPage, parents, rssLinks, out);
            
           // System.out.println(links.size() + " links extracted");
            
			// Convert each link text to a url and enque
			for (Map.Entry<String, String> entry : links.entrySet()) {
				try {
					String url = entry.getValue();
					// urls might be relative to current page
					URL link = new URL(url);
									   
					// If layers are not used, write everything into same layer
					if (tc.getMaxLevel() == -1)
						queue.push(link, level);
					else
						queue.push(link, level + 1);
				} catch (MalformedURLException e) {
					// Ignore malformed URLs, the link extractor might
					// have failed.
				}
			}
			
			//System.out.println(rssLinks.size() + " rsss links");
			//System.out.println(rssLinks);
			
			out.close();
			
		} catch (Exception e) {
			 e.printStackTrace();
			// process of this object has failed, but we just ignore it here
		}
	}
}
